module.exports = {
    "env": {
        "es6": true,
        "node": true,
        "mocha": true
    },
    "parserOptions": {
        "ecmaVersion": 2017
    },
    "extends": "standard",
    "rules": {
        "quotes": ["error", "single"],
        "no-var": ["error"],
        "prefer-const": ["error"],
        "arrow-spacing": ["warn"],
    }
};
