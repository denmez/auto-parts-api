const app = module.exports = require('express')()

// server status
app.get('/', (req, res) => {
  res.json({ msg: 'hello! Server is up and running' })
})

app.use('/auth', require('./auth'))
