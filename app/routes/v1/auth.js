const app = module.exports = require('express')()

app.get('/sign-in', (req, res) => {
  res.json({
    te: 'sign-in'
  })
})

app.get('/sign-up', (req, res) => {
  res.json({
    te: 'sign-up'
  })
})
