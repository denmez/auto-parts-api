api:
	nodemon

docker:
	sudo docker-compose up --build

migration_up:
	npm run migrate latest

migration_down:
	npm run migrate rollback

migration_create:
	npm run migrate make ${name}

test:
	npm test
