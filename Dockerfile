FROM node:carbon

RUN mkdir -p /usr/src/api

WORKDIR /usr/src/api

COPY package.json /usr/src/api/

RUN npm install

COPY . /usr/src/api/

EXPOSE 3333
CMD [ "node", "app/index.js" ]
